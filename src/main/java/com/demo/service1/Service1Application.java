package com.demo.service1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.lang.invoke.MethodHandles;

@SpringBootApplication
@RestController
public class Service1Application {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	@Autowired
	RestTemplate restTemplate;
	@Value("${service2.address:localhost:8082}") String service2;

	public static void main(String[] args) {
		SpringApplication.run(Service1Application.class, args);
	}
	@RequestMapping("/")
	public String frontPage() throws InterruptedException {
		log.info("Front Page");
		return "Front Page";
	}

	@RequestMapping("/demostart")
	public String demostart() throws InterruptedException {
			log.info("Welcome To Service1. Calling Service2");
			//String response = restTemplate.getForObject("http://" + service2 + "/startOfService2", String.class);
			//Adding for testing, remove after
			String response = restTemplate.getForObject("https://gitlab-microservices-demo-service2.34.66.106.66.nip.io/startOfService2", String.class);
			log.info("Got response from service2 [{}]", response);
			return response;

	}

	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}



}
